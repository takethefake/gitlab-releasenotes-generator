const fetch = require("node-fetch");
const config = require("./config");
const colors = require("colors");

const paramsToQuery = params =>
  Object.keys(params)
    .map(key => key + "=" + params[key])
    .join("&");

const urls = {
  tags: (projectId, params) =>
    `${config.baseUrl}/projects/${projectId}/repository/tags?${paramsToQuery(
      params
    )}`,
  single_tag: (projectId, tagName, params) =>
    `${
      config.baseUrl
    }/projects/${projectId}/repository/tags/${tagName}?${paramsToQuery(
      params
    )}`,
  mergeRequests: (projectId, params) =>
    `${config.baseUrl}/projects/${projectId}/merge_requests?${paramsToQuery(
      params
    )}`,
  mergeRequestsCommits: (projectId, mergeRequestId, params) =>
    `${
      config.baseUrl
    }/projects/${projectId}/merge_requests/${mergeRequestId}/commits?${paramsToQuery(
      params
    )}`
};

const defeaultHeaders = {
  "Private-Token": config.gitlabAccessToken
};

const defaultParams = {
  per_page: 100
};

const fetchEndpointJson = async (
  url,
  method = "get",
  headers = defeaultHeaders
) => {
  return await fetch(url, { method, headers })
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response;
    })
    .then(res => res.json())
    .catch(err => console.log(err));
};

const fetchAll = async urlGenerator => {
  let results = [];
  let page = 1;
  let fetchEndpoint = true;
  while (fetchEndpoint) {
    const response = await fetchEndpointJson(urlGenerator(page));
    results = [...results, ...response];
    page = page + 1;
    fetchEndpoint = response.length === 100;
  }
  return results;
};

exports.fetchCurrentTag = async () => {
  const tags = await fetchEndpointJson(
    urls.tags(config.projectId, {
      ...defaultParams,
      sort: "desc",
      order_by: "updated"
    })
  );
  return tags[0];
};

exports.fetchSingleTag = async tagName => {
  const tag = await fetchEndpointJson(
    urls.single_tag(config.projectId, tagName, {})
  );
  return tag;
};

renderMergeRequest = async (mr, detailed, describe) => {
  if (detailed) {
    const commits = await fetchAll(page =>
      urls.mergeRequestsCommits(config.projectId, mr.iid, { page })
    );
    console.log(colors.green(mr.title));
    commits.map(commit => {
      console.log("    " + commit.title);
    });
    console.log("");
  }else if(describe) {
    console.log(colors.green(mr.title));
    console.log("    " + mr.description.replace(/\n\r?/g, '\n\t'));
  }else{
    console.log(colors.green(mr.title));
  }
};

exports.fetchAllMergedMergeRequestsBetweenDate = async (
  startdate,
  enddate,
  branch,
  detailed = false,
  describe = false
) => {
  target_branch = branch ? branch : "develop";
  const allMrs = await fetchAll(page =>
    urls.mergeRequests(config.projectId, {
      ...defaultParams,
      updated_after: startdate,
      updated_before: enddate,
      state: "merged",
      order_by: "created_at",
      target_branch,
      page
    })
  );
  allMrs.map(mr => renderMergeRequest(mr, detailed,describe));
};
