#!/usr/bin/env node

const gitlab = require("./gitlab");
const program = require("commander");
const moment = require("moment");

const mr = async cmd => {
  let fromDate = null;
  let endDate = null;
  if (cmd.startdate) {
    fromDate = moment(cmd.startdate).format("YYYY-MM-DD");
  }
  if (cmd.enddate) {
    fromDate = moment(cmd.enddate).format("YYYY-MM-DD");
  }
  if (cmd.lastTag) {
    const tag = await gitlab.fetchCurrentTag();
    if (tag.commit) {
      fromDate = tag.commit.created_at;
    }
  }
  if (cmd.starttag) {
    const tag = await gitlab.fetchSingleTag(cmd.starttag);
    if (tag.commit) {
      fromDate = tag.commit.created_at;
    }
  }
  if (cmd.endtag) {
    const tag = await gitlab.fetchSingleTag(cmd.endtag);
    if (tag.commit) {
      endDate = tag.commit.created_at;
    }
  }
  let branch = null;
  if (cmd.branch) {
    branch = cmd.branch;
  }
  if (!fromDate) {
    const tag = await gitlab.fetchCurrentTag();
    if (tag.commit) {
      fromDate = tag.commit.created_at;
    }
  }
  if(!endDate) {
    endDate =  new Date().toISOString();
  }
  gitlab.fetchAllMergedMergeRequestsBetweenDate(
    fromDate,
    endDate,
    branch,
    cmd.showcommits,
    cmd.describe,
  );
};

program
  .version("0.0.1")
  .description("gets all merge Request between certain commits/tags")
  .option(
    "--starttag <tag>",
    "get all MergeRequest starting at a specified Tag"
  )
  .option(
    "--endtag <tag>",
    "get all MergeRequest till a specified Tag"
  )
  .option("-l, --last-tag", "get all MergeRequest between now and last Tag")
  .option("-c, --showcommits", "Shows all assigned Commits to a merge Request")
  .option(
    "--startdate <date>",
    "get all MergeRequest after a certain ISO-Date (YYYY-MM-DD)"
  )
  .option(
    "--enddate <date>",
    "get all MergeRequest until a certain ISO-Date (YYYY-MM-DD)"
  )
  .option(
    "--describe",
    "describes the merge request"
  )
  .option("-b, --branch <branch>", "merge request on branch")
  .action(mr);

program.parse(process.argv);
