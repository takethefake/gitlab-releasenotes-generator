# Gitlab Merge Request Command

Usage: node index.js [options]

gets all merge Request between certain commits/tags

Options:

    -V, --version          output the version number
    --starttag <tag>       get all MergeRequest starting at a specified Tag
    --endtag <tag>         get all MergeRequest till a specified Tag
    -l, --last-tag         get all MergeRequest between now and last Tag
    -c, --showcommits      Shows all assigned Commits to a merge Request
    --startdate <date>     get all MergeRequest after a certain ISO-Date (YYYY-MM-DD)
    --enddate <date>       get all MergeRequest until a certain ISO-Date (YYYY-MM-DD)
    --describe             describes the merge request
    -b, --branch <branch>  merge request on branch
    -h, --help             output usage information

## get all updated and merged MergeRequests between lastTag and current Develop

> node index.js


## get all updated and merged MergeRequest between two Tags on Develop

> node index.js --starttag v1.23.0 --endtag v1.24.0

## get all updated and merged MergeRequest between a Tag and a Date on Develop

> node index.js --starttag v1.23.0 --enddate 2018-08-01


## get descriptions for all merge Requests in a defined spn

> node index.js --describe

## get all containing commits from MergeRequests

> node index.js --showcommits


## show all MergeRequests on Master including their commits since a specified date

> node index.js --startdate 2018-07-01 --branch master --showcommits

